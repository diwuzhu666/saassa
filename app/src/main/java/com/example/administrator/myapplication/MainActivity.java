package com.example.administrator.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private TextView chenshi;
    private TextView tianqi;
    private TextView danqian;
    private TextView fenli;
    private TextView shidu;
    private EditText houquchenshi;
    private Button chaxun;
    private Button genduo;
    private TextView shauxin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       chenshi = (TextView) findViewById(R.id.chenshi);
        tianqi = (TextView) findViewById(R.id.tianqixianxiang);
        danqian = (TextView) findViewById(R.id.dianqianqiwen);
        fenli = (TextView) findViewById(R.id.fengli);
        shidu = (TextView) findViewById(R.id.shidu);
        houquchenshi = (EditText)findViewById(R.id.chenshichaxun);
        chaxun = (Button)findViewById(R.id.chaxun);
        genduo = (Button) findViewById(R.id.genduo);
        shauxin = (TextView)findViewById(R.id.shauxin);
        tianqiguiy();

        shauxin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tianqiguiy();
            }
        });

        chaxun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tianqi();
            }
        });
        genduo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Genduo.class);
                startActivity(intent);
            }
        });
    }
   public void tianqi (){
       RequestQueue requestQueue = Volley.newRequestQueue(this);
       String chen = houquchenshi.getText().toString();
       StringRequest stringRequest = new StringRequest("http://api.yytianqi.com/observe?city="+chen+"&key=ro2bi5kq5lqo399b", new Response.Listener<String>() {
           @Override
           public void onResponse(String response) {
               try {
                   JSONObject jsonObject = new JSONObject(response);
                   String caty = jsonObject.getString("data");
                   JSONObject data = new JSONObject(caty);
                   String cityName = data.getString("cityName");
                   String tq = data.getString("tq");//天气现象
                   String qw = data.getString("qw");//当前气温
                   String fl = data.getString("fl");//当前风力
                   String sd = data.getString("sd"); //相对湿度，直接在此数值后添加%即可
                   chenshi.setText(cityName);
                   tianqi.setText(tq);
                   danqian.setText(qw);
                   shidu.setText(sd+"%");
                   fenli.setText(fl);


               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
       }, new Response.ErrorListener() {
           @Override
           public void onErrorResponse(VolleyError error) {

           }
       });
       requestQueue.add(stringRequest);
       requestQueue.start();

   }

    public void tianqiguiy(){

        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        String chen = houquchenshi.getText().toString();
        StringRequest stringRequest = new StringRequest("http://api.yytianqi.com/observe?city=CH260101&key=ro2bi5kq5lqo399b", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String caty = jsonObject.getString("data");
                    JSONObject data = new JSONObject(caty);
                    String cityName = data.getString("cityName");
                    String tq = data.getString("tq");//天气现象
                    String qw = data.getString("qw");//当前气温
                    String fl = data.getString("fl");//当前风力
                    String sd = data.getString("sd"); //相对湿度，直接在此数值后添加%即可
                    chenshi.setText(cityName);
                    tianqi.setText(tq);
                    danqian.setText(qw);
                    shidu.setText(sd+"%");
                    fenli.setText(fl);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
        requestQueue.start();



    }
}
